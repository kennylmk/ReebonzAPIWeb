<?php

		escapeSpecialCharacter($strEscape)
        {
            $strEscape = str_replace("'", "\'", $strEscape);
            $strEscape =str_replace('""', '\"', $strEscape);
            $strEscape = str_replace("&", "\&", $strEscape);

            return $strEscape;
        }

 	// List<string[]> getMerchantDescTemplateToList($filePath, $merchantId, $mappingElement, $getValueAtNextLine = false)
  //   {
  //           XDocument xd;
  //           XElement node;

  //               xd = XDocument.Load($filePath);
  //               node = xd.Root.Elements("Merchant").FirstOrDefault(x => x.Element("Id").Value.Trim() == $merchantId.Trim());
  //               if (node != null)
  //               {
  //                   nodeMapping = node.Element($mappingElement);
  //                   mappingSettingList = new List<string[]>();
  //                   //mappingSettingList = new List<KeyValuePair<string, string>>();
  //                   foreach (var nodeChild in nodeMapping.Descendants())
  //                   {
  //                       string $templateValue = "";
  //                       string typeValue = "";
  //                       string translated = "";
  //                       string attribute = "";

  //                       foreach (var attr in nodeChild.Attributes())
  //                       {
  //                           if ($getValueAtNextLine)
  //                           {
  //                               if (attr.Name == "Template") $templateValue = attr.Value;
  //                               if (attr.Name == "Type") typeValue = attr.Value;
  //                               if (attr.Name == "Translated") translated = attr.Value;
  //                               if (attr.Name == "Attribute") attribute = attr.Value;
  //                               //if (attr.Name == "ValueAtNextLine") $templateValue = attr.Value;
  //                           }
  //                           else
  //                           {
  //                               if (attr.Name == "Template") typeValue = attr.Value;
  //                               if (attr.Name == "Type") $templateValue = attr.Value;
  //                               if (attr.Name == "Translated") translated = attr.Value;
  //                               if (attr.Name == "Attribute") attribute = attr.Value;
  //                               //if (attr.Name == "Template") $templateValue = attr.Value;
  //                           }
  //                       }

  //                      if ($templateValue !=null && $templateValue !="") mappingSettingList.Add(new string[] { $templateValue, translated, typeValue, attribute });
  //                   }

  //                   return mappingSettingList;
  //               }
  //               else
  //                   return null;

  //   }
     
        // isSentence(strCheck, parts = null)
        // {
        //     //string delimSentence = "を,に,は,が,です";
        //     string[] delimSentence = txtSentenceIndicators.Text.Trim().Split(',');
        //     foreach (var s in delimSentence)
        //     {
        //         if (delimSentence.Count() == 1 && string.IsNullOrEmpty(s.Trim())) return false;
        //         if (strCheck.Contains(s)) return true;
        //     }
        //     return false;
        // }


        typeLineData($lineData,$ENtoJP=1)
        {

                $delimTitle = ":,：,「,【,『,［";

                //Title and value - BEFORE -> ":,：,「,【,『,［"    &&   NOT を,に,は,が,です,で,※,コメント,鑑定士コメント
                if (strpos($lineData,$delimTitle) )
                    return "TITLEVALUE";
                //Sentences
                // else if (isSentence($lineData))
                //     return descTranslateDataType.SENTENCE;
                else if (!isJapanese($lineData))
                    if ($ENtoJP==2)
                        return "OTHER";
                    else
                        return "NONJAPANESE";
                else
                    return "SENTENCE";

        }

        isJapanese($str)
        {
            if (isset($str))
            {
               return preg_match('/[\x{4E00}-\x{9FBF}\x{3040}-\x{309F}\x{30A0}-\x{30FF}]/u',$str);
            }
            else
            {
                return false;
            }
        }



        handleLineDataSENTENCE($item, $lstAllDel,$ENtoJP=1)
        {
            $strDefaultDelim = "：,、,。,・・・";

            //Split Header and item 

                // string[] delimSentence = txtSentenceIndicators.Text.Trim().Split(',');
                foreach ($strDefaultDelim as $s)
                {
                    $splitHeaderAndSentense = preg_split( @"(" + $s + ")",$item);
                    if (count($splitHeaderAndSentense) > 1)
                    {
                        $header = $splitHeaderAndSentense[1];
                        $item = $splitHeaderAndSentense[2];
                        break;
                    }

                }

                if (isset($header))
                {
                    //Get Header Delimter 
                    foreach ($lstAllDel as $s)
                    {
                        $splitHeader = preg_split(@"(?<=(" + $s + "))",$header);
                        if (count($splitHeader) > 1) $strDelim = $splitHeader[1];
                    }

                    //Get translated Header
                    //$header = LocateWordinXML(dictPath, header.Replace(strDelim, ""),ENtoJP);
                    //if (header == null)
                    //{
                        $header = goTranslate($header, $ENtoJP);
                    //}
                    //Get replaced Delimiter
                    //$strDelim = $lstAllDel.Where(v => v.Key == strDelim).Select(v => v.Value).ToList()[0] ?? "";
                }

                //Split Sentence
                $parts = preg_split(@"(?<=[：、。に　])",$item); //Split sentence

                //Splited sentence in Array , Add NUMBERING
                $cnt = 1;
                foreach ($parts as $extractPart)
                {
                    partsPair.Add(cnt + "_" + extractPart, "");
                    cnt += 1;
                }

                string[] arraySentence = new string[parts.Count()];

                //Loop Splited Sentence in Array with NUMBERING
                for (int i = 0; i < partsPair.Count(); i++)
                {
                    var strWithoutNo = partsPair.ElementAt(i).Key.Split('_').ToList(); //Get Splited word from Single Array item withoutN NUMBERING
                    if (!string.IsNullOrEmpty(strWithoutNo[1]))
                    {
                        //Locate the extracted data from Dictionary
                        string strDic = null;

                        //Find from Existing XML Dict
                        strDic = LocateWordinXML(dictPath, strWithoutNo[1].Trim(), ENtoJP);

                        if (strDic == null) //Not found in dictionary then go for translator
                        {
                            string strTranslated = null;
                            strTranslated = goTranslate(strWithoutNo[1].Trim(), ENtoJP);

                            partsPair[partsPair.ElementAt(i).Key] = strTranslated;
                            arraySentence[i] = strTranslated;
                        }
                        else //Found in dictionary
                        {
                            partsPair[partsPair.ElementAt(i).Key] = strDic;
                            arraySentence[i] = strDic;
                        }

                        if (partsPair.ElementAt(i).Key.Contains("。")) arraySentence[i] = "\n";
                        if (parts[i].Contains("。")) arraySentence[i] = "\n";
                    }
                }


                string strHold = "";
                //add every sentence to array
                List<string> arraySplittedSentences = new List<string>();

                //************************ RECONTRUCT SENTENCE *****************************

                for (int r = 0; r <= partsPair.Count() - 1; r++)
                {
                    var strCurrentKey = partsPair.ElementAt(r).Key.Split('_').ToList()[1].Trim();
                    var strCurrentValue = partsPair.ElementAt(r).Value;

                    if (strCurrentKey == "" || strCurrentKey == null) continue;

                    if (!isSentence(strCurrentKey) && partsPair.Count() == 1)
                    {
                        arraySplittedSentences.Add(strCurrentValue + "\n");
                    }
                    else if (strCurrentKey.Contains("に") || !conditionWords.Any(strCurrentKey.Contains) && !"が,：,。,※,は,、".Any(strCurrentKey.Contains) && !Regex.IsMatch(strCurrentKey.Substring(strCurrentKey.Length - 1, 1), @"\d+") && r != partsPair.Count() - 1)
                    {

                        if (strCurrentKey.Contains("に"))
                        {
                            strHold = strCurrentValue + (strHold == "" ? "" : ", ") + strHold;
                        }
                        else
                        {
                            strHold += (strHold == "" ? "" : " ") + strCurrentValue + (strCurrentKey.Contains("、") ? "," : "");
                        }

                    }
                    else
                    {
                        if (r + 1 <= partsPair.Count() - 1 && strCurrentKey != null && strCurrentKey != "")
                        {
                            var strNextKey = partsPair.ElementAt(r + 1).Key.Split('_').ToList()[1];

                            if (((strHold != "" && !conditionWords.Any(strNextKey.Contains)) || "が,所々".Any(strNextKey.Contains)) && !Regex.IsMatch(strCurrentKey.Substring(strCurrentKey.Length - 2, 1), @"\d+"))
                            {
                                strSentence += strCurrentValue + " " + strHold + ("：".Any(strCurrentKey.Contains) ? ":" : ". ");
                                strSentence = capitalFirstLetterOfSentence(strSentence.ToLower()).Replace(",.", ".");

                                arraySplittedSentences.Add(strSentence);
                                strHold = "";
                                strSentence = "";
                            }
                            else if (strHold == "" && strCurrentKey.Contains("が") && !strCurrentKey.Contains("。"))
                            {
                                strSentence += strCurrentValue + ", ";
                            }
                            else if (r + 1 == partsPair.Count - 1 && !conditionWords.Any(strNextKey.Contains) && !strCurrentKey.Contains("。"))
                            {
                                strSentence += strCurrentValue + ", ";
                            }
                            else if (strHold == "" && !conditionWords.Any(strNextKey.Contains) && strNextKey.Contains("に") && !strCurrentKey.Contains("："))
                            {
                                strHold += (strHold == "" ? "" : " ") + strCurrentValue;
                            }
                            else if (strHold == "" && !conditionWords.Any(strNextKey.Contains)) //Next is NOT contains シワ","キズ","スレ",　"ハガレ","シミ",　"サビ",　"ホツレ",　"ヘコミ",　"クスミ",　"押し跡",　"メッキ剥げ","ヨゴレ"

                            {
                                strSentence += strCurrentValue + ("：".Any(strCurrentKey.Contains) ? ":" : ". ");
                                strSentence = capitalFirstLetterOfSentence(strSentence.ToLower()).Replace(",.", ".");
                                arraySplittedSentences.Add(strSentence);
                                strHold = "";
                                strSentence = "";
                            }
                            else if (strHold == "" && "：".Any(strCurrentKey.Contains) || "が".Any(strNextKey.Contains)) //Next is NOT contains シワ","キズ","スレ",　"ハガレ","シミ",　"サビ",　"ホツレ",　"ヘコミ",　"クスミ",　"押し跡",　"メッキ剥げ","ヨゴレ"

                            {
                                strSentence += strCurrentValue + ("：".Any(strCurrentKey.Contains) ? ":" : ". ");
                                strSentence = capitalFirstLetterOfSentence(strSentence.ToLower()).Replace(",.", ".");
                                arraySplittedSentences.Add(strSentence);
                                strHold = "";
                                strSentence = "";
                            }

                            else if (strCurrentKey.Contains("。"))
                            {
                                strSentence += strCurrentValue + " " + strHold + ". ";
                                strHold = "";
                            }
                            else if (!conditionWords.Any(strCurrentKey.Contains) && !Regex.IsMatch(strCurrentKey, @"\d+"))
                            {
                                strHold += (strHold == "" ? "" : " ") + strCurrentValue;
                            }
                            else
                            {
                                strSentence += strCurrentValue + ", ";
                            }
                        }
                        else
                        {
                            if (strCurrentKey != null && strCurrentKey != "")
                            {
                                strSentence += strCurrentValue + " " + strHold + ". ";
                                strSentence = capitalFirstLetterOfSentence(strSentence.ToLower()).Replace(",.", ".");
                                arraySplittedSentences.Add(strSentence);
                                strHold = "";
                                strSentence = "";
                            }

                        }
                    }
                }

                strSentence = header + strDelim + " " + string.Join(" ", arraySplittedSentences);

                return strSentence + "\n";
           
        }

    startTranslation(data, titleTranslateOnly = false,$ENtoJP=1)
    {
                $str = data;
                $str = str_replace("<BR>", "\n",$str);
                $str = str_replace("<br>", "\n",$str);
                $str = str_replace("<\br>", "\n",$str);
                $str = str_replace("<br />", "\n",$str);
                $str = str_replace("</br>", "\n",$str);

                //Remove HTML Tags
                $str = preg_eplace("<.*?>", "",$str );

                //Escape ' " & / //
                $str = escapeSpecialCharacter($str);
                $lstLine = explode('\n',$str);

                //Delimiters List to split single line data
                $lstAllDel = array();
                $lstAllDel["　"]= " ";
                $lstAllDel["："]= ":";
                $lstAllDel[":"] = ":";
                $lstAllDel["・・・"]= ":";

                //$string[] lstDel = $lstAllDel.Select(p => p.Key).ToArray();

                //GET DESCRIPTION TEMPLATE IN Merchant.xml
                //descTemplateList = getMerchantDescTemplateToList(merchantSettingPath, selectedMerchantId, "DescriptionTemplates");

                //*********************** ROW - Line Data *******************************
                foreach (var item in $lstLine)
                {
                    // if (txtSkip.Text.Any(item.Contains)) //SKIP TEXT
                    // {
                    //     continue;
                    // }
                    
                    //FOUND EXIST DECRIPTION => EXTRACT VALUE 
                    // var foundInTemplateList = (item != null && descTemplateList !=null ? findValueInArray(descTemplateList, 2, item) : null);

                    //IS MERCHANT , DESCRIPTION TEMPLATE NOT EMPTY
                    // if (selectedMerchantId != null && descTemplateList != null && (foundInTemplateList != null) && !$string.IsNullOrEmpty(item))
                    // {
                    //     $itemValue = item.Replace(foundInTemplateList[2], "");
                    //     if (isJapanese($itemValue))
                    //     {
                    //         $string $strDic = LocateWordinXML(dictPath, $itemValue,$ENtoJP);
                    //         if ($strDic == null || $strDic == "") //Not found in dictionary then google translate
                    //         {
                    //             $strDic = goTranslate($itemValue + " ",$ENtoJP);
                    //         }
                    //         $itemValue = $strDic;
                    //     }
                    //     formRetEN .= foundInTemplateList[1] + " " + $itemValue + "\n";
                    // }
                    // else//NOT MERCHANT => NORMAL TRANSLATION
                    // {
                        if (isset(item) )
                        {
                            //lblStatus2.Invoke(new Action(() => lblStatus2.Text = formRetEN));
                            switch (typeLineData(item, $ENtoJP))
                            {
                                case "SENTENCE":
                                    formRetEN .= handleLineDataSENTENCE(item, $lstAllDel, $ENtoJP);
                                    break;

                                case "TITLEVALUE":
                                    formRetEN .= handleLineDataTITLEVALUE(item, lstDel, $lstAllDel, $ENtoJP) + "\n";
                                    break;

                                // case descTranslateDataType.TITLE:
                                //     formRetEN .= handleLineDataTITLE(item, chk$ENtoJP.Checked ? 2 : 1);
                                //     break;
                                case "NONJAPANESE":
                                    formRetEN .= item + "\n";
                                    break;
                                default:
                                    formRetEN .= handleLineDataTITLEVALUE(item, lstDel, $lstAllDel, $ENtoJP) + "\n";
                                    break;
                            }
                        }
                        // else if (isset(item) && isset(titleTranslateOnly) //TRANSLATE TITLE ONLY
                        // {
                        //     formRetEN .= handleTITLE(item,$ENtoJP);
                        // }
                        else //NO DATA FOR TRANSLATION and RETURN EMPTY 
                            formRetEN .= "\n"; //LINE data is NULL or EMPTY                        
                    // }
                }
                return formRetEN;
   }
?>