<?php
include "callAPI.php";
ini_set('max_execution_time',5000); //3000 seconds = 50 minutes

//echo 'Call to API >>'.$_POST['productcode'];

$update_after = $_POST['updateafter'];
$page_size= $_POST['pagesize'];

echo '<br>Update after:'.$update_after;
echo '<br>Page size:'.$page_size;

$endpoint= 'http://api.reebonz.com/gate';
$product_list_url = '/api/wasabi/ois_product_list';
$product_qty_url = '/api/wasabi/ois_product_qty';
$parameter = '?updated_at_after='.$update_after.'&start=0&rows='.$page_size;

echo 'URL = '.$endpoint.$product_qty_url.$parameter.'<p>';

$response = CallAPI("GET",$endpoint.$product_list_url.$parameter);
$json = json_decode($response,true);

$numFound=$json['response']['numFound']; 
echo '<br>Total:'.$numFound;
$totalPages = intval($numFound/$page_size);

	//var_dump($json);
	echo '<br>Total Pages:'.$totalPages;
	echo '<br><a href="product_qty_list.htm">Return to previous</a>';

	echo '<table border=1>';
	echo '<tr>';
	//****************** HEADER ********************
	echo '<td>No</td>';
	echo '<td>Page</td>';
	echo '<td>SKU</td>';
	echo '<td>Item No.</td>';
 	echo '<td>Item No.</td>';
	echo '<td>Size Code</td>';
	echo '<td>Size Label</td>';
	echo '<td>Qty</td>';
	echo '<td>Updated</td>'; 
	echo '</tr>';

	//*************** CSV　HEADER************/
	$csv = 'No,';
	$csv .= 'Page,';
	$csv .= 'SKU,';
	$csv .= 'Item No.,';
 	$csv .= 'Item No.,';
	$csv .= 'Size Code,';
	$csv .= 'Size Label,';
	$csv .= 'Qty,';
	$csv .= 'Updated'; 
	$csv .=PHP_EOL;
	$cnt=1;
for ($page=0; $page <=$totalPages; $page++)
{
	$parameter = '?updated_at_after='.$update_after.'&start='.$page.'&rows='.$page_size;
	$response = CallAPI("GET",$endpoint.$product_list_url.$parameter);
	$json = json_decode($response,true);
	$doc = $json['response']['docs'];

	if (isset($doc))
	{		
		foreach ($doc as $value)
		{
		
			$responseQty = CallAPI("GET",$endpoint.$product_qty_url.'?sku='.$value['sku']);
//echo $endpoint.$product_qty_url.'?sku='.$value['sku'];
//echo '<br>'.$responseQty;
			$json2 = json_decode($responseQty,true);
			
			$docQty = $json2['response']['docs'];
//var_dump ($docQty);
			foreach ($docQty as $valueQty)
			{
				echo '<tr>';
				echo '<td>'.$cnt.'</td>';
				$csv .=$cnt.',';
				echo '<td>'.$page.'</td>';
				$csv .=$page.',';
				echo '<td>'.$valueQty['sku'].'</td>';
				$csv .=$valueQty['sku'].',';
				echo '<td>'.$valueQty['item_id'].'</td>';
				$csv .=$valueQty['item_id'].',';
				echo '<td>'.$valueQty['item_no'].'</td>';
				$csv .=$valueQty['item_no'].',';
				echo '<td>'.$valueQty['size_code'].'</td>';
				$csv .=$valueQty['size_code'].',';
				echo '<td>'.$valueQty['size_label'].'</td>';
				$csv .=$valueQty['size_label'].',';
				echo '<td>'.$valueQty['qty'].'</td>';
				$csv .=$valueQty['qty'].',';
				echo '<td>'.$valueQty['updated_at'].'</td>'; 
				$csv .=$valueQty['updated_at'].',';
				echo '</tr>';
				$csv .=PHP_EOL;
				$cnt +=1;
			}
		}
	}
}

echo '</table>';
$myfile = fopen("./cache/productqty_".date("dmYhis").".csv", "w") or die("Unable to open file!");
fwrite($myfile, $csv);
fclose($myfile);
echo '<br><a href="./cache/productqty_".date("dmYhis").".csv">Download CSV</a>';
echo '<br><a href="product_qty_list.htm">Return to previous</a>';
messagebox("Completed.");
?>