<?php
	include "callAPI.php";
	ini_set('max_execution_time',5000); //3000 seconds = 50 minutes

	$update_after = $_POST['updateafter'];
	$page_size= $_POST['pagesize'];
	$page=$_POST['page'];
	$rank_status=$_POST['rank'];
	// echo $rank_status;
	/* READ CSV FILE */
	// $csv_mimetypes = array('text/csv', 'text/plain', 'application/csv', 'text/comma-separated-values', 'application/excel', 'application/vnd.ms-excel', 'application/vnd.msexcel', 'text/anytext', 'application/octet-stream', 'application/txt');
	// if (in_array($_FILES['file']['type'], $csv_mimetypes)) 
	// {
	// 	 Grab the location of this PHP script and change the path to a different location where we can save the data 
	// 	$filePathRaw = dirname(__FILE__);
	// 	$filePathSegments = explode("/", $filePathRaw);
	// 	for ($x = 1; $x < (sizeof($filePathSegments) - 3); $x++) 
	// 	{
	// 		$filePath = $filePath . "/" . $filePathSegments[$x];
	// 	}

	// 	/* Generate a filename for the CSV file */
	// 	$token = date("YmdHis");
	// 	/* Save the CSV data */
	// 	$rawCSV = file_get_contents($_FILES['file']['tmp_name']);
	// 	$lines=explode("\n",$rawCSV);
	// }
	/* END OF CSV */

	$endpoint= 'http://api.reebonz.com/gate';
	$product_list_url = '/api/wasabi/ois_product_list';
	$product_query_url = '/api/wasabi/ois_product_info';
	$product_qty_url = '/api/wasabi/ois_product_qty';

	$parameter = '?updated_at_after='.$update_after.'&start='.$page.'&rows='.$page_size;

	echo 'URL = <a href='.$endpoint.$product_list_url.$parameter.'</a><p>';
	echo 'Rank status:'.$rank_status;

	$response = CallAPI("GET",$endpoint.$product_list_url.$parameter);
	$json = json_decode($response,true);
	$numFound=$json['response']['numFound']; 
	echo '<br>Total:'.$numFound;
	$totalPages = intval($numFound/$page_size);

	echo '<br>Total Pages:'.$totalPages;
	echo '<br>Page:'.$page;
	echo '<br><a href="product_qty_list.htm">Return to previous</a>';
	echo '<table border=1>';

	echo '<tr>';
	echo '<td>No</td>';
	// echo '<td>Sku</td>';
	// echo '<td>Brand:</td>';
	// echo '<td>Brand(jp):</td>';
	// echo '<td>Brandcode(jp):</td>';
	// echo '<td>Parent Category:</td>';
	// echo '<td>Parent Category(jp):</td>';
	// echo '<td>Sub Category:</td>';
	// echo '<td>Sub Category(jp):</td>';
	// echo '<td>Updated AT:</td>';
	// echo '<td>Title:</td>';
	// echo '<td>Title(jp):</td>';
	// echo '<td>Description:</td>';
	// echo '<td>Description(jp):</td>';
	// echo '<td>Currency(JPY):</td>';
	// echo '<td>Retail Price (JPY):</td>';
	// echo '<td>Selling Price (JPY):</td>';
	// echo '<td>Color:</td>';
	// echo '<td>Color(jp):</td>';
	// echo '<td>Color Codes:</td>';
	// echo '<td>Color Codes(jp):</td>';
	// echo '<td>Gender:</td>';
	// echo '<td>Gender(jp):</td>';
	// echo '<td>item_no:</td>';
	// echo '<td>item_id:</td>';
	// echo '<td>size_code:</td>';
	// echo '<td>size_label:</td>';
	// // echo '<td>size_label(jp):</td>';
	// echo '<td>qty:</td>';
	// echo '<td>updated_at(QTY):</td>';
	// echo '<td>Images:</td>';
	echo '<td>path</td>';
	echo '<td>name</td>';
	echo '<td>code</td>';
	echo '<td>original-price</td>';
	echo '<td>price</td>';
	echo '<td>headline</td>';
	echo '<td>caption</td>';
	echo '<td>color</td>';
	echo '<td>delivery</td>';
	echo '<td>product-category</td>';
	echo '<td>rank</td>';
	echo '<td>vintage_grade_rating</td>';
	echo '<td>vintage_grade_title_en</td>';
	echo '<td>vintage_grade_rarity_title</td>';
	echo '<td>weight_en</td>';	
	echo '</tr>';
	
//	path	name	code	original-price	price	headline	caption	brand-code	delivery	product-category	display
	
	$html_start="<div class=r_item><table cellspacing=1 cellpadding=8 border=0>";
	$html_end = "</table></div>";

	//$women_size_chart ="<br><br><br><img width=700 src="https://shopping.c.yimg.jp/lib/reebonzjp/womenshoesziechart.jpg">

	$header_open="<tr><th width=120 bgcolor=#f5f5f5 align=left>";
	$header_close="</th><td width=450 bgcolor=#ffffff align=left>";
	$footer="</td></tr>";

	$brand_header=$header_open."ブランド".$header_close;
	$code_header=$header_open."品番".$header_close;
	$caption_header=$header_open."仕様".$header_close;
	$color_code=$header_open."カラー".$header_close;
	$measurement_header = $header_open."測定/サイス/寸法".$header_close;
	$material_header=$header_open."素材".$header_close;


	$csv="";
	$csv_header="";
	//option csv
	//code	sub-code	option-name-1	option-value-1
	$csv_option ='code,';
	$csv_option .= 'sub-code,';
	$csv_option .= 'option-name-1,';
	$csv_option .= 'option-value-1';
	$csv_option .=PHP_EOL;

	//quantity csv
	//code	sub-code	mode	quantity
	$csv_qty = 'code,';
	$csv_qty.= 'sub-code,';
	$csv_qty.= 'mode,';
	$csv_qty.= 'quantity';
	$csv_qty .=PHP_EOL;


	$parameter = '?updated_at_after='.$update_after.'&start='.$page.'&rows='.$page_size;
	$response = CallAPI("GET",$endpoint.$product_list_url.$parameter);
	$json = json_decode($response,true);
	$doc = $json['response']['docs'];
	
	if (isset($doc))
	{	$cnt = 1;
		foreach ($doc as $value)
		{
			$responseQty = CallAPI("GET",$endpoint.$product_qty_url."?sku=".$value['sku']);
			$jsonQty = json_decode($responseQty,true);
			$docQty = $jsonQty['response']['docs'];

			foreach ($docQty as $valueQty)
			{
				if ($rank_status=="new"){
					// if ($valueQty['qty']>0 )//Included Pre-owned
					if ($valueQty['qty']>0 && $value['vintage_grade_title_en']=="")//Brand new
					{
						$dataList=buildData($value,$valueQty,$cnt,$csv,$csv_option,$csv_qty);
						$csv=$dataList['csv'];
						$csv_option=$dataList['csv_option'];
						$csv_qty=$dataList['csv_qty'];
						$cnt++;
					}
				}
				else if ($rank_status=="pre-owned"){
									// if ($valueQty['qty']>0 )//Included Pre-owned
					if ($valueQty['qty']>0 && $value['vintage_grade_title_en']!="")//Pre-owned
					{
						// $dataList=buildData($value,$valueQty,$cnt,$csv,$csv_option,$csv_qty);
						$dataList=buildData($value,$valueQty,$cnt,$csv,$csv_option,$csv_qty);
						$csv=$dataList['csv'];
						$csv_option=$dataList['csv_option'];
						$csv_qty=$dataList['csv_qty'];
						$cnt++;
					}
				}
				else
				{
					if ($valueQty['qty']>0 )//ALL
					{
						// $dataList=buildData($value,$valueQty,$cnt,$csv,$csv_option,$csv_qty);
												$dataList=buildData($value,$valueQty,$cnt,$csv,$csv_option,$csv_qty);
						$csv=$dataList['csv'];
						$csv_option=$dataList['csv_option'];
						$csv_qty=$dataList['csv_qty'];
						$cnt++;
					}	
				}
			}
		}			
	}
	echo '</table>';

//CSV HEADER
	$csv_header='path,';
	$csv_header .='name,';
	// $csv_header .='brand_header,';
	// $csv_header .='brand,';
	// $csv_header .='brand_footer,';
	// $csv_header .='code_header,';
	$csv_header .= 'code,';
	// $csv_header .='code_footer,';
	$csv_header .= 'original-price,';
	$csv_header .= 'price,';
	$csv_header .= 'headline,';
	// $csv_header .='caption_header,';
	$csv_header .= 'caption,';
	// $csv_header .='caption_footer,';
	// $csv_header .='explanation,';
	$csv_header .= 'brand-code,';
	$csv_header .= 'delivery,';
	$csv_header .=  'product-category,';
	$csv_header .=  'display,';
	// $csv_header .='html_start,';
	$csv_header .=  'description_en,';
	$csv_header .=  'color,';
	// $csv_header .='color_jp_header,';
	$csv_header .=  'color_jp,';
	// $csv_header .='color_jp_footer,';
	$csv_header .=  'weight_en,';
	$csv_header .=  'weight_jp,';
	$csv_header .=  'measurement_en,';
	// $csv_header .='measurement_jp_header,';
	$csv_header .=  'measurement_jp,';
	// $csv_header .='measurement_jp_footer,';
	$csv_header .=  'material_en,';
	// $csv_header .=  'material_jp_header';
	$csv_header .=  'material_jp,';
	$csv_header .=  'rank';
	$csv_header .= 'sp-additional'; //MOBILE DISPLAY
	// $csv_header .=  'material_jp_footer';
	// $csv_header .=  'html_end';

	$csv_header .=PHP_EOL;



	//$DownloadPath="C:\Users\Kenny\Desktop\yahoo up 27042018\NewIn";
	$DownloadPath="./cache";
	$DownloadProductCSV = $DownloadPath."/productlist_".date("dmYhis").".csv";
	$DownloadOptionCSV = $DownloadPath."/productoptionlist_".date("dmYhis").".csv";
	$DownloadQuantityCSV = $DownloadPath."/productqtylist_".date("dmYhis").".csv";

	//Product csv
	$myfile = fopen($DownloadPath."/productlist_".date("dmYhis").".csv", "w") or die("Unable to open file!");
	fwrite($myfile, mb_convert_encoding($csv_header.$csv, 'Shift_JIS'));
	fclose($myfile);

	//Product Opton csv
	$myfile = fopen($DownloadPath."/productoptionlist_".date("dmYhis").".csv", "w") or die("Unable to open file!");
	fwrite($myfile, mb_convert_encoding($csv_option, 'Shift_JIS'));
	fclose($myfile);

	//Product Quantity csv
	$myfile = fopen($DownloadPath."/productqtylist_".date("dmYhis").".csv", "w") or die("Unable to open file!");
	fwrite($myfile, mb_convert_encoding($csv_qty, 'Shift_JIS'));
	fclose($myfile);

	messagebox("Completed");

	echo '<br><a href="'.$DownloadProductCSV.'">Download Product CSV</a>';
	echo '<br><a href="'.$DownloadOptionCSV.'">Download Option CSV</a>';
	echo '<br><a href="'.$DownloadQuantityCSV.'">Download Quantity CSV</a>';
	echo '<br><a href="product_qty_list.htm">Return to previous</a>';

function buildData($value,$valueQty,$cnt,$csv,$csv_option,$csv_qty)
{
		// echo '<tr>';
	// echo '<td>'.$cnt.'</td>';
	// echo '<td>'.$value['sku'].'</td>';	$csv .= $value['sku'].',';
	
	$code=$value['sku'];
	//echo '<td>'.$value['brand'].'</td>'; $csv .=  $value['brand'].',';
	$brand=$value['brand'];
	$brand_jp=CallGoogleTranslateAPI("en","ja",$value['brand']);
	//echo '<td>'.$brand_jp.'</td>'; $csv .=  $brand_jp.',';
	//echo '<td></td>'; $csv .=  ',';
	$parent_category=$value['parent_category'];
	// echo '<td>'.$value['parent_category'].'</td>'; $csv .= $value['parent_category'].',';
	$product_category="";
	switch ($value['parent_category']) {
	    case "BAGS":
	        $parent_category_jp="バッグ";

			// if ($value['gender_en'][0]=="Male"){
			// 	$product_category
			// }
			// else if($value['gender_en'][0]=="Female")
			// {
			// 	$product_category
			// }
	        break;
	    case "APPAREL":
	        $parent_category_jp="洋服";
	        break;
	    case "SMALL LEATHER GOODS":
	        $parent_category_jp="革小物";
	        break;
	    case "JEWELLERY":
	        $parent_category_jp="ジュエリー";
	        break;
	    case "ACCESSORIES":
	        $parent_category_jp="アクセサリー";
	        break;
	    case "SHOES":
	        $parent_category_jp="シューズ";
	        break;

	}
	// echo '<td>'.$parent_category_jp.'</td>'; $csv .= $parent_category_jp.',';

	
	 // echo '<td>'.$value['sub_category'].'</td>'; $csv .= $value['sub_category'].',';
	 $sub_category=$value['sub_category'];
	 $sub_category_jp = CallGoogleTranslateAPI("en","ja",$value['sub_category']);
	 // echo '<td>'.$sub_category_jp.'</td>'; $csv .= $sub_category_jp.',';
 	 $update_at=$value['updated_at'];
	 // echo '<td>'.$value['updated_at'].'</td>'; 	$csv .=  $value['updated_at'].',';
	
	 $name=$value['title_en'];
	 // echo '<td>'.$value['title_en'].'</td>'; $csv .= '"'.$value['title_en'].'",';

	 $title_jp=CallGoogleTranslateAPI("en", "ja",$value['title_en']);
	 // echo '<td>'.$title_jp."<br>";$csv .= '"'.$title_jp.'",';

	 $description=preg_replace("/\r\n|\r|\n|\"/", "<br>",$value['description_en']);
	 // echo '<td>'.$value['description_en'].'</td>'; $csv .= '"'.preg_replace("/\r\n|\r|\n/", "<br>",$value['description_en']).'",';
	$caption_jp_br="";
	$caption_jp_rn="";
	$lines=explode("\n",$value['description_en']);

	if (isset($lines)){
		 foreach ($lines as $linevalue) {
		 	if (isset($linevalue)){
				$caption_jp_br.=CallGoogleTranslateAPI("en", "ja",$linevalue)."<br>";
				$caption_jp_rn.= $linevalue."\r\n";
		 	}
		 	}				
	}
	$caption='"'.$caption_jp_br.'"';
	 // echo "<td>".$caption_jp_br."</td>"; $csv .= '"'.$caption_jp_br.'",';
	 // echo "<td>".$caption_jp_rn."</td>";
	$currency_jp=$value['currency_jp'];
	$retail_price_jp=$value['retail_price_jp'];
	$selling_price_jp=$value['selling_price_jp'];
	// echo '<td>'.$value['currency_jp'].'</td>';	$csv .=  $value['currency_jp'].',';
	// echo '<td>'.$value['retail_price_jp'].'</td>'; 	$csv .= $value['retail_price_jp'].',';
	// echo '<td>'.$value['selling_price_jp'].'</td>'; $csv .=  $value['selling_price_jp'].',';

	// echo '<td>';
	if (isset($value['color_en']))
	{
		$color_jp="";
		$color="";
		foreach ($value['color_en'] as $color_en)
		{
			 // $filter_color=str_replace("(Designer Colour)","", $color_en);
			$filter_color=$color_en;
			// echo $filter_color.';';	
			$color .=   $filter_color.';';
			
			$color_jp.= CallGoogleTranslateAPI("en", "ja",$filter_color).' x ';
			
		}
		$color=substr($color,0,-1);
		$color_jp=substr($color_jp,0,-3);
	}
	// echo '</td>'; 	$csv .=',';

	// $color_jp= CallGoogleTranslateAPI("en", "ja",$value['color_en']);
	// echo '<td>';
		// echo $color_jp.';';	$csv .=   $color_jp.';';
	// echo '</td>'; 	$csv .=',';

	if (isset($value['color_codes']))
	{
		$color_codes=$value['color_codes'][0];
		// echo '<td>'.$value['color_codes'][0].'</td>';  $csv .=  $value['color_codes'][0].',';
		 $color_codes_jp=CallGoogleTranslateAPI("en", "ja",$value['color_codes'][0]);
	}
	// echo '</td>';$csv .=',';

 	// echo '<td>'.$color_codes_jp.'</td>';  $csv .=  $color_codes_jp.',';
	   
   // echo '<td>';
	if (isset($value['gender_en']))
	{
		$gender_en=$value['gender_en'][0];
		// echo  $value['gender_en'][0]; $csv .=  $value['gender_en'][0].'';

		if ($value['gender_en'][0]=="Male"){
			$gender_jp="メンズ";
		}
		else if($value['gender_en'][0]=="Female")
		{
			$gender_jp="ウィメンズ";
		}
		else
		{
			$gender_jp="";
		}
	}


	$weight_en=$value['weight_en'];
	$weight_jp=CallGoogleTranslateAPI("en", "ja",$value['weight_en']); 

	$measurement_en='';
	$measurement_jp='';
	$measurement_en_list=explode("\r\n",$value['measurement_en']);
	foreach ($measurement_en_list as $measurement)
	{
		$measurement_en .=$measurement.',';
		$measurement_jp .=CallGoogleTranslateAPI("en", "ja",$measurement).',';
	}
	$measurement_en= substr($measurement_en,0,-1);
	$measurement_jp= substr($measurement_jp,0,-1);


	$material_en='';
	$material_jp='';
	foreach ($value['material_en'] as $material)
	{
		$material_en .=$material.',' ;
		$material_jp .=CallGoogleTranslateAPI("en", "ja",$material).','; 
	}
	$material_en = substr($material_en, 0,-1);
	$material_jp = substr($material_jp, 0,-1);

	// $csv .=  ','; 	echo '</td>';

	// echo  '<td>'.$gender_jp.'</td>'; 	$csv .= $gender_jp.',';

	$item_id=$valueQty['item_id'];
	$sub_code=$valueQty['item_no'];
	$size_code=$valueQty['size_code'];
	$size_label=$valueQty['size_label'];
	// echo '<td>'.$valueQty['item_id'].'</td>';  $csv .=  $valueQty['item_id'].',';
	// echo '<td>'.$valueQty['item_no'].'</td>';  $csv .=  $valueQty['item_no'].',';
	// echo '<td>'.$valueQty['size_code'].'</td>';  $csv .=  $valueQty['size_code'].',';
	// echo '<td>'.$valueQty['size_label'].'</td>';  $csv .=  $valueQty['size_label'].',';

	$qty=$valueQty['qty'];
	$qty_update_at =$valueQty['updated_at'];
	// echo '<td>'.$valueQty['qty'].'</td>';  $csv .=  $valueQty['qty'].',';
	// echo '<td>'.$valueQty['updated_at'].'</td>';  $csv .=  $valueQty['updated_at'].',';

	$rank=$value['vintage_grade_title_en'];
	$imageslist='';
	foreach ($value['images'] as $image)
	{
		// var_dump($image);
		$imageslist.=$image.';';
		// echo '<a href='.$image.'>'.$image.'</a><br>'; $csv .=  $image.';';
	}
	$imageslist = substr($imageslist,0,-1);
	// echo '</td>';
	// echo '</tr>'; $csv .=PHP_EOL;



	//echo "<br>".$csv;

	//Product CSV
	echo "<tr>";
	echo "<td>".$cnt.'</td>';
	echo "<td>".$gender_jp.":".$parent_category_jp.'</td>';
	if ($value['vintage_grade_title_en']==""){
		echo "<td>".'【新品】　'.$name.' '.$title_jp.'</td>';
	}
	else if ($value['vintage_grade_title_en']!=""){
		echo "<td>".'【中古】　'.$name.' '.$title_jp.'</td>';
	}

	
	echo "<td>".$code.'</td>';
	echo "<td>".$retail_price_jp.'</td>';
	echo "<td>".$selling_price_jp.'</td>';
	echo "<td>".'送料無料　'.$brand_jp.' '.$parent_category_jp.','.$sub_category_jp.'</td>';
	echo "<td>".$caption.'</td>';
	echo "<td>".$color.'</td>';
	//echo "<td>".'</td>';//$brand-code,';//*************
	echo "<td>".'1</td>';//delivery
	echo  "<td>".$product_category.'</td>';//'product-category,';
	echo  "<td>".$rank.'</td>';//display

	echo  "<td>".$value['vintage_grade_rating'].'</td>';//display
	echo  "<td>".$value['vintage_grade_title_en'].'</td>';//display
	echo  "<td>".var_dump($value['vintage_grade_rarity_title_en']).'</td>';//display
	echo  "<td>".$value['weight_en'].'</td>';//display
	echo "</tr>";

	//BRAND NEW or PRE-OWNED
	$csv .= $gender_jp.":".$parent_category_jp.',';
	if ($value['vintage_grade_title_en']==""){
		$csv .= '【新品】　'.$name.' '.$title_jp.',';		
	}
	else if ($value['vintage_grade_title_en']!=""){
		$csv .= '【中古】　'.$name.' '.$title_jp.',';	
	}

	//MOBILE DISPLAY
	$sp_additional=$caption;


	$csv .= $code.',';
	$csv .= $retail_price_jp.',';
	$csv .= $selling_price_jp.',';
	$csv .= '送料無料　'.$brand_jp.' '.$parent_category_jp.' '.$sub_category_jp.',';
	$csv .= $caption.',';
	$csv .= ',';//$brand-code,';//*************
	$csv .= '1,';//delivery
	$csv .=  $product_category.',';//'product-category,';
	$csv .=  '1,';//display

	//ADDITIONAL INFO
	// $csv .=  $name.',';//name eng
	$csv .=  '"'.$description.'",';
	$csv .=  $color.',';
	$csv .=  $color_jp.',';
	$csv .=  '"'.$weight_en.'",';
	$csv .=  '"'.$weight_jp.'",';
	$csv .=  '"'.$measurement_en.'",';
	$csv .=  '"'.$measurement_jp.'",';
	$csv .=  '"'.$material_en.'",';
	$csv .=  '"'.$material_jp.'",';
	$csv .=  '"'.$rank.'"';	

	$csv .=PHP_EOL;

	//option csv
	//code	sub-code	option-name-1	option-value-1
	$csv_option .=$code.',';
	$csv_option .= $sub_code.',';
	if (isset($size_label))
	{
		$csv_option .= 'サイス,';
		$csv_option .= $size_label;						
	}
	else
	{
		$csv_option .= 'カラー,';
		$csv_option .=  $color_jp;			
	}

	$csv_option .=PHP_EOL;

	//quantity csv
	//code	sub-code	mode	quantity
	$csv_qty.= $code.',';
	$csv_qty.= $sub_code.',';
	$csv_qty.= ',';
	$csv_qty.= $qty;
	$csv_qty.=PHP_EOL;

	return array('csv'=> $csv,
				'csv_option'=> $csv_option,
				'csv_qty'=> $csv_qty);
}
?>